#include <stdio.h>
#include <stdlib.h>
#include "collatz.h"

int main( void )
{
  int matrix[1048576], sum;

  for (int i = 0; i <= 1048576; i++ ) {
    matrix[i]  = i + 1;
    matrix[i] = collatz( matrix[i] );
  }

  for ( int i = 0; i <= 262143; i++ ) {

    sum = 0;

   for ( int j = i * 4 ; j <= i * 4 + 3; j++ ) {
     sum += matrix[j];
   }
   printf("Sum [%d - %d] is %d\n", i * 4 + 1, (i + 1) * 4, sum);
  }

  return 0;
}
