#include <stdio.h>
#include <stdlib.h>

enum boolean{false, true};

// function main begins program execution 
int main( void )
{

  for( int limit = 4; limit <= 8; limit *= 2) {
    
    int cociente[limit],
      residuo[limit];

    for( int i = 1; i <= limit; i++ )
      cociente[i - 1] = limit;

    for ( int i = 1; i <= limit; i++)
      residuo[i - 1] = i % limit;

    printf("\t d = %d\n", limit);
    for ( int i = 1; i <= limit; i++)
      printf("%dq + %d\n\n", cociente[i - 1], residuo[i - 1] );

    enum boolean status = true;
    for ( int n = 1; status; n++) {

      printf("\n\n\t\tCycle %d\n", n );

      for ( int i = 1; i <= limit; i++) {

	if( residuo[i - 1] % 2 == 0 && cociente[i - 1] % 2 == 0 )
	  residuo[i - 1] /= 2, cociente[i - 1] /= 2;
	else
	  residuo[i - 1] = residuo[i - 1] * 3 + 1, cociente[i - 1] *= 3;

	printf("%dq + %d\n", cociente[i - 1], residuo[i - 1] );

	if( residuo[i - 1] % 2 == 0 && cociente[i - 1] % 2 == 0 )
	  printf("0\n");
	else if( residuo[i - 1] % 2  == 1 && cociente[i - 1] % 2 == 0 )
	  printf("1\n");
	else
	  printf("error\n");

	for( int m = 1; m <= limit; m++)
	  if( cociente[m - 1] % 2 == 1)
	    status = false;
      }


    }

  }

  return 0;
}
