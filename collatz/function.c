#include <stdio.h>
#include <stdlib.h>
#include "collatz.h"

int main( void )
{
  int a;

  printf( "Enter an Integer: " );
  scanf( "%d", &a );

  printf("%d \n\n", collatz(a));
}
