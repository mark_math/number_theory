#include <stdio.h>
#include <math.h>
#include "collatz.h"

int main( void )
{
  int limit = pow(2, 19), a[limit], pattern = 2;

  for( int i = 0; i <= limit - 1; i++ )
    a[i] = i + 1;

  for( int cycle = 1; cycle <= 18; cycle++ ) {

    for( int i = 0; i <= limit - 1; i++ )
      a[i] = collatz( a[i] );

    pattern *= 2;
    int b[pattern];

    printf("\t\tCycle %d\n", cycle );

    for( int i = 0; i <= pattern - 1; i++ )
      b[i % pattern] = a[i] % 2;

    int x, y;

    for(  int i = pattern; i <= limit - 1; i++ ) {
        x = a[i] % 2, y = b[ i % pattern ];
        printf( "%d\t\t%d\t%d\t", a[i], x, y );

        if( x == y )
          puts( "sí" );
        else
          {
            puts( "no" );
            i = limit;
          }
      }

    puts( "\n\n" );
  }

}
