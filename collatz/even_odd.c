// Cuenta el número de pares e impares de cada patrón
#include <stdio.h>
#include <math.h>
#include "collatz.h"

int main( void )
{
   int cycles = 17,
     current_cycle = 1,
     limit = pow(2, cycles + 1),
     pattern = 2,
     a[limit];

  for(  int i = 1; i <= limit; i++)
      a[i] = i;

  while( current_cycle <= cycles )
    {
      printf( "\t\tCiclo %d\n", current_cycle );
      for(  int i = 1; i <= limit; i++)
	a[i] = collatz( a[i] );

      pattern *= 2;
      int b[pattern], even = 0;

      for(  int i = 1; i <= pattern; i++) {
	  b[ i % pattern] = a[i] % 2;
	  if( ( b[ i % pattern ] % 2) == 0 )
	    even++;
	}

      printf("Impar: %d\n", pattern - even );
      printf("Par: %d\n", even);

      printf("\n\n");

      current_cycle++;
    }

  return 0;
}
