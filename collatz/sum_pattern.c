#include <stdio.h>
#include <math.h>
#include "collatz.h"

int main( void )
{
  int limit;
  long long sum;

  for (int cycle = 1; cycle <= 18; cycle++ ) {
    sum = 0, limit = pow(2, cycle + 1);

    int number[limit][2];

    for( int i = 0; i <= limit - 1; i++ )
      number[i][0] = limit, number[i][1] = (i + 1) % limit;

    for (int i = 1; i <= cycle; i++)
      for (int j = 0; j <= limit - 1; j++)
        if( number[j][0] % 2 == 0 && number[j][1] % 2 == 0 )
          number[j][0] /= 2, number[j][1] /= 2;
        else
          number[j][0] *= 3, number[j][1] = number[j][1] * 3 + 1;

    for( int i = 0; i <= limit - 1; i ++ )
      sum += number[i][0];

    printf("Sum of patter %d° = %lli\n", cycle, sum );

  }
  return 0;
}
