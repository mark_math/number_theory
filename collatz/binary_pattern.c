#include <stdio.h>
#include <math.h>
#include "collatz.h"

int main( void )
{
  int limit = pow(2, 19), a[limit], pattern = 2;

  for( int i = 1; i <= limit; i++ )
    a[i - 1] = i;

  for( int cycle = 1; cycle <= 18; cycle++ ) {

    for( int i = 1; i <= limit; i++ )
      a[i - 1] = collatz( a[i - 1] );

    pattern *= 2;
    int b[pattern];

    printf("\t\tCycle %d\n", cycle );

    for( int i = 1; i <= pattern; i++ ) {
      b[i % pattern - 1] = a[i - 1] % 2;
      printf("%d\n", b[i % pattern - 1] );
    }

    puts( "\n\n" );
  }

}
