int collatz(  int a )
{
  return ( a % 2 == 0 ? a / 2 : a * 3 + 1 );
}
